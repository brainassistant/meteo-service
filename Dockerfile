FROM python:3.7

USER root

RUN apt-get update \
    && apt-get install -y --no-install-recommends \
        postgresql-client \
        uwsgi \
        uwsgi-plugin-python3 \
    && rm -rf /var/lib/apt/lists/*

WORKDIR /opt/
ADD . /opt/src/
WORKDIR /opt/src/
ENV HOME /opt/src/

RUN chown -R www-data:www-data /opt/src/
USER www-data
COPY requirements.txt ./
CMD RUN /usr/local/bin/python manage.py collectstatic --noinput


USER root
RUN pip3 install --no-cache-dir -r requirements.txt
CMD RUN /usr/local/bin/python manage.py migrate

EXPOSE 8000

# CMD tail -f /dev/null
CMD uwsgi -i /opt/src/etc/uwsgi.ini
