import random


class TemperatureMixin(object):
    """
    TemperatureMixin - is addition function which may convert one temperature value to another
    and format result with numbers after comma
    """
    valid_keys = {
        'K': 'Kelvin',
        'F': 'Fahrenheit',
        'C': 'Celsius'
    }

    @staticmethod
    def round_random_tie_breaking(value: float, after_comma: int = 1):
        """
        A random value has been added to emulate a 'Random tie-breaking',
        see more https://en.wikipedia.org/wiki/Rounding
        """
        result = value

        # Limit for python3.6 exponent was 323, after returned 0.0
        if after_comma > 300:
            assert "To match value for round_by_random, breaking > 300."

        # split number bu dot 32.233 -> ['32', '235']
        if type(value) is float:
            split_num = str(value).split('.')
            # if rounding less then digits after comma
            if len(split_num[1]) > after_comma:
                # if last digit is 5
                if split_num[1] == '5':
                    # len last part 233 -> 3
                    # rounded by 2 is 23 + random 0 or 0.1^3 is 0.001 = 236
                    # round(32.236, 2) = 32.24
                    result = round(value + random.choice([0, 0.1 ** len(split_num[1])]), after_comma)
                else:
                    result = round(value, after_comma)

        # Round return float like 312.0
        if after_comma == 0:
            result = int(result)
        return result

    @classmethod
    def convert_temp(cls,
                     value: float,
                     from_val: str = None,
                     to_val: str = None,
                     exact_value: bool = False,
                     after_comma: int = 0):
        """
        Convert method - convert one temperature value to another.

        Its working for C->F, C->K
        if exact_value == True, after_comma does not working

        :return:
        """
        result = None
        if (from_val is None and to_val is None) or from_val == to_val:
            # Works like rounding
            result = value
        elif from_val == 'C' and to_val == 'F':
            result = 1.8 * value + 32
        elif from_val == 'C' and to_val == 'K':
            result = value + 273.15
        # elif from_val == 'K' and to_val == 'C':
        #     pass
        # elif from_val == 'K' and to_val == 'F':
        #     pass
        # elif from_val == 'F' and to_val == 'K':
        #     pass
        # elif from_val == 'F' and to_val == 'C':
        #     pass
        elif from_val not in cls.valid_keys and to_val not in cls.valid_keys:
            raise f"Wrong value to convert {from_val} or {to_val}"

        if exact_value:
            return cls.round_random_tie_breaking(result, 0)
        else:
            return cls.round_random_tie_breaking(result, after_comma)
