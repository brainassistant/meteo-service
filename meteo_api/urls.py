from django.urls import path, include
from rest_framework import routers
from . import api

app_name = 'polls'
router = routers.SimpleRouter()

# Routers apis.
router.register(r'temperature', api.TempApi)

urlpatterns = router.urls
