import re
from datetime import datetime

from rest_framework import serializers

from . import models
from . mixins import TemperatureMixin


class TemperatureModelSerializer(serializers.ModelSerializer, TemperatureMixin):

    class Meta:
        model = models.TemperatureModel
        lookup_field = 'date_time'

        fields = (
            'date_time',
            'place',
            'temp_value'
        )

        extra_kwargs = {
            'date_time': {'read_only': True}
        }

    @staticmethod
    def bool_convert(val):
        x = val
        if type(val) != bool:
            x = None
            if val.lower() in ['true', 'yes', '1']:
                x = True
            elif val.lower() in ['false', 'no', '0']:
                x = False
        return x

    def to_representation(self, instance):
        """
        For method GET return scale of temperature what requested
        scale - what is temperature scale F, C or K
        exact - what values need, rounded without after comma numbers, or just rounded
        """
        rep = super().to_representation(instance)
        if self.context['request'].method == 'GET':
            scale = self.context['request'].query_params.get('scale', None)
            exact = self.bool_convert(self.context['request'].query_params.get('exact', True))
            after_comma = int(self.context['request'].query_params.get('after_comma', 0))

            if scale is not None:
                rep['temp_value'] = self.convert_temp(rep['temp_value'],
                                                      self.Meta.model.temp_value_default_scale,
                                                      scale,
                                                      exact_value=exact,
                                                      after_comma=after_comma)
                rep['scale'] = scale
            else:
                # Just formatting, without converting.
                rep['temp_value'] = self.convert_temp(rep['temp_value'],
                                                      exact_value=exact,
                                                      after_comma=after_comma)
                rep['scale'] = self.Meta.model.temp_value_default_scale
            return rep
