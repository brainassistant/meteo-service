import random as rd
from django.db import models


class TemperatureModel(models.Model):
    """
    As a default scale value of temperature set the 'temp_value_default_scale', it is inherited in a linked classes.
    """
    temp_value_default_scale = 'C'

    class Meta:
        unique_together = ('date_time', 'temp_value', 'place')

    id = models.AutoField(
        primary_key=True,
    )

    date_time = models.DateTimeField(
        verbose_name='Date and time',
        blank=False,
    )

    temp_value = models.FloatField(
        blank=False,
        default=False,
    )

    place = models.CharField(
        blank=False,
        max_length=20,
        default=False,
        verbose_name='Place'
    )
