# Python libs.
from datetime import datetime


# Additional libs
from rest_framework import viewsets, \
                           status, \
                           serializers
# Package imports.
from . import serializer, \
              models


class TempApi(viewsets.ModelViewSet):
    """
    request params:
    host/api/temperature?date_time=2018-02-20 11:00&
                         date_time_range=2018-02-20 11:00,2019-02-20 11:00&
                         [scale=F&]
                         [after_comma=2&]
                         place=Moscow


    response api:
    {
    'date_time': 20-02-2016 14:00,
    'place': Moscow,
    'temperature': 31.20,
    'scale': F
    },
    {
    'date_time': 20-02-2016 15:00,
    'place': Moscow,
    'temperature': 27.34,
    'scale': F
    },
    {...}
    """
    queryset = models.TemperatureModel.objects.all()
    serializer_class = serializer.TemperatureModelSerializer
    allowed_temp_tags = ['F', 'K', 'C']

    def validate(self, data):
        """
        Validator for new fields, return information about wrong cases
        """
        validation_errors = []

        for field, val in data.items():
            if len(val) == 0:
                validation_errors.append(f"{field}: cant be empty, set value or delete params")

        if data.get('date_time', False):
            errors = 0
            for single_data in data.get('date_time').split(','):
                try:
                    datetime.strptime(single_data, '%Y-%m-%d %H:%M:%S')
                except ValueError:
                    errors += 1

                try:
                    datetime.strptime(single_data, '%Y-%m-%d')
                except ValueError:
                    errors += 1

            if errors > len(data.get('date_time').split(',')):
                validation_errors.append("Date_time: wrong format, use 'YYYY-MM-DD HH:MM:SS'\\'YYYY-MM-DD' "
                                         + "or same for range with comma 'YYYY-MM-DD,YYYY-MM-DD'")

        if data.get('scale', False):
            print('Type', type(data.get('scale', False)))
            if not data['scale'].isalpha() or data['scale'] not in self.allowed_temp_tags:
                validation_errors.append("scale: must be 'K', 'F' ('C' as default)")

        if data.get('after_comma', False):
            if not data['after_comma'].isdigit():
                validation_errors.append("after_comma: must be digit")
            elif int(data['after_comma']) > 300:
                validation_errors.append("after_comma: Too match numbers after comma, set below 300")

        # If date exists
        exact = data.get('exact', None)
        if exact is not None:
            # If existing data cant converted to bool
            if self.serializer_class.bool_convert(exact) is None:
                validation_errors.append("exact: Exact value must be bool instance")

        if len(validation_errors) > 0:
            raise serializers.ValidationError(validation_errors)
        return data

    def get_queryset(self):
        """
        Filtering by fields
        """
        self.validate(self.request.query_params)

        date_time = self.request.query_params.get('date_time', False)
        place = self.request.query_params.get('place', False)
        single_date_len = 0
        if date_time:
            # Formatting and create datetime objects
            date_time = date_time.split(',')
            date = []
            for r_date in date_time:
                # YYYY-MM-DD 14:12:59
                if len(r_date.split(' ')) > 1:
                    single_date_len += 1
                    date.append(datetime.strptime(r_date, '%Y-%m-%d %H:%M:%S'))
                else:
                    date.append(datetime.strptime(r_date, '%Y-%m-%d'))

            if len(date) > 1:
                self.queryset = self.queryset.filter(date_time__range=(date[0], date[1]))

            elif len(date) == 1:
                # YYYY-MM-DD 14:12:59
                if single_date_len > 0:
                    self.queryset = self.queryset.filter(date_time=datetime.strptime(date[0], '%Y-%m-%d %H:%M:%S'))
                else:
                    date = datetime.strptime(date[0], '%Y-%m-%d')
                    self.queryset = self.queryset.filter(date_time__year=date.year,
                                                         date_time__month=date.month,
                                                         date_time__day=date.day)

        if place:
            self.queryset = self.queryset.filter(place=place.lower())
        return self.queryset
